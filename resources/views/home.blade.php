@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default" style="border-color:#FACA04">
                <div class="panel-heading" style="color:#fff; border-color:#FACA04" >Dashboard</div>
                <div class="panel-body" style="background-color:#000; color:#fff;">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                      Welcome to our glorious store!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
