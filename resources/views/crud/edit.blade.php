@extends('layouts.app')

@section('content')
<h1 style="text-align:center;color:white">Editar Produto</h1>

<form action="/crud/{{$product->id}}" method="post">
  <input name="_method" type="hidden" value="PUT"/>

{{csrf_field()}}
<div class="col-md-6 col-md-offset-3">

  Nome:<input type="text" class="form-control" name="name" value="{{$product->name}}">
  <br>
  Preço:<input type="text" class="form-control" name="price" value="{{$product->price}}">
  <br>
<!--  Categoria:<input type="text" class="form-control" name="category" value="{{$product->category}}"> -->
Categoria:  <select id="categories" class="form-control" name="categories">
    @foreach($categories as $category)
      <option {{ ($category->id==$product->category_id)?'selected':'' }} value="{{$category->id}}" class="form-control">
        {{$category->name}}
      </option>
    @endforeach
</select>

  <br>
  Imagem: <input type="text" class="form-control" name="image" value="{{$product->image}}">
  <br>
  Descrição: <input type="text" class="form-control" name="description" value="{{$product->description}}">
  <br>
  <input type="submit" class="btn" style="background-color:#FACA04; color:#000" value="Update"/>
</div>
@stop
