@extends('layouts.app')
@section('content')
<h1 style="text-align: center"> Produtos </h1>
@foreach($products->chunk(3) as $productChunk)
    <div class="col-md-12" style="margin-left: 23%;margin-right: 23%;">
    @foreach ($productChunk as $product)
          <div class="thumbnail col-sm-2" style="border-color:yellow; margin:10px; background-color:#f6f6f6">
              <img src="{{ asset('/img/' .$product->image)}}" alt="image" class="img-responsive"/>
              <div class="caption">
                  <h3>{{$product->name}}</h3>
                  <p class="description"> {{$product->description}}</p>
                  <div class="clearfix">
                      <div class="pull-left price">{{$product->price}}€</div>
                      <a href="{{ route('product.addToCart', ['id' => $product->id]) }}" class="btn pull-right" style="background-color:#000; color:#FFF"role="button">Add to Cart</a>
                    </div>
                  </div>
              </div>
          @endforeach
        </div>
@endforeach
@stop
