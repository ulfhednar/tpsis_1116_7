@extends('layouts.app')

@section('content')

<form action="/crud" enctype="multipart/form-data" method="post" class="col-md-12">

  {{csrf_field()}}
<div class="col-md-6 col-md-offset-3">
    <h1 style="text-align:center;color:white">Inserir novo producto</h1>

Nome:  <input type="text" class="form-control" name="name"/>
  <br/>
Preço:  <input type="text" class="form-control" name="price"/>
  <br/>
Categoria:  <select id="categories" class="form-control" name="categories">
    @foreach($categories as $category)
      <option
        type="hidden"  name="category" value="{{$category->id}}" class="form-control">
        {{$category->name}}
      </option>
    @endforeach
</select>
  <br/>
Imagem:  <input id="image" type="file" name="image">
  <br/>
Descrição: <input type="text" class="form-control" name="description"/>
  <br/>
  <input type="submit" class="btn" style="background-color:#FACA04; color:#000" value="Create">
</div>
</form>

@stop
