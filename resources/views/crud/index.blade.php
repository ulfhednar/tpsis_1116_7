@extends('layouts.app')
@section('content')
<table class="table" style="width:50%; margin:0 25% 0 25%; color:#fff">
  <thead>
    <a href='/crud/create' style="width:50%; margin:0 45% 0 45%">Inserir Novo Produto</a>
    <p>
  </thead>
      <tr>
      <th>Nome</th>
      <th>Preço</th>
      <th>category</th>
      <th>Descrição</th>
      <th></th>
    </tr>
  @foreach($products as $product)
    <tbody>
  <tr>
    <th>{{$product->name}}</th>
    <td>{{$product->price}}</td>
    <td>{{ $product->category->name }}</td>
    <td>{{$product->description}}</td>
    <td>
    <a href="/crud/{{ $product->id }}/edit">Edit</a>
      <form action="/crud/{{ $product->id }}" method="post">
        {{ method_field('DELETE') }}
        {{ csrf_field() }}
        <input type="submit" class="btn" style="background-color:#FACA04; color:#000" value="Delete"/>
      </form>
    </td>
  </tr>
    <tbody>
  @endforeach
</table>
@stop
