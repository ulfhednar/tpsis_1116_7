@extends('layouts.app')
@section('content')

  <div class="container">
    <h1>PC Master Race</h1>
    <blockquote  style="border-left: 5px solid #FACA04; color:#FFF">
<p>
  The PC Master Race, sometimes referred to by its original phrasing as the Glorious PC Gaming Master Race, is an internet subculture, internet community, and a tongue-in-cheek term of superiority for PC gaming used among gamers to compare PC gaming to console gaming.<br>

  In current parlance, the term is used by PC enthusiasts both to describe themselves as a group, as well as their belief in the superiority of the PC platform in comparison to consoles, often citing features like more advanced graphics, smoother framerates, free online play, backwards compatibility, modifications, upgradability, customization, lower cost-over-time, open standards, multitasking, and performance.<br>
  Popular imagery, discussion, and media referencing the term also commonly describes console users who find console better than pc as "console peasants" and people who play on PC as the "Glorious PC Gaming Master Race".
</p>
  <footer>PC Master Race</footer>
    </blockquote>
  </div>
@stop
