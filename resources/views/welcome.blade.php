<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                  <div class="title">
                    PC Master Race
                  </div>
                <div class="sub-title">
                  MAY OUR FRAMERATES BE HIGH AND OUR TEMPERATURES LOW
                  <div class="title m-b-md">
                      PC Master Race
                </div>
                <h5>May our frame rates be high and our temperatures low!</h5>
            </div>
        </div>
    </body>
</html>
