<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Auth;
use File;
use DB;
use Illuminate\Supports\Facades\Input;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = product::all();
      return view('crud.index',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if (Auth::user() == null)
        return view('layouts.app');
      if (Auth::user()->role_id==1)
        $categories = Category::all();
        return view('crud.insert',['categories'=>$categories]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
              'name' => 'required',
              'price' =>'required|numeric'
            ]);
            $product = new Product();
            $product->name = $request->get('name');
            $product->price = $request->get('price');
            $pic_name = $_FILES['image']['name'];
            $new_path = public_path() . '/img/' . $pic_name ;
            move_uploaded_file($_FILES['image']['tmp_name'],$new_path);
            $product->description = $request->get('description');
            $product->category_id = $request->get('categories');
            $product->image = $pic_name;
            $product->save();

            return redirect('/crud');
            //Product::create()
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $product = Product::find($id);
      $categories = Category::all();
      return view('crud.edit',['product'=>$product,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::find($id);
        $product->name = $request->get('name');
        $product->price = $request->get('price');
        $product->image = $request->get('image');
        $product->description = $request->get('description');
        $product->category_id = $request->get('categories');

        $product->save();
        return redirect('/crud/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('/crud');
    }
}
