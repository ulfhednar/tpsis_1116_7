<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

/**
 *
 */
class PagesController extends Controller
{

  function home()
  {
    return view('home');
  }

  function contact()
  {
    return view('contact');
  }

  function about()
  {
    return view('about');
  }

  function index()
  {
    return view('index');
  }

function show()
{
  $products = Product::all();
  return view('crud.show',compact('products'));
  }
}


 ?>
