<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users') -> insert([
          'name' => 'Admin',
          'email' => 'admin@pcmr.pt',
          'password' => Hash::make('atec2017'),
          'role_id' => 1
      ]);
    }
}
