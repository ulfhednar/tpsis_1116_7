<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');

});
*/
//Route::get('/home', 'PagesController@home');
//Route::get('/about', 'PagesController@about');
//Route::get('/contact', 'PagesController@contact');
//Route::get('/index', 'PagesController@index');

Route::resource('/crud','PostsController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
Route::get('admin/posts/example', array('as'=>'admin.home' ,function(){

    $url = route('admin.home');

    return "This url is: ". $url;

}));
*/

Route::get('/', function()
{
    return View('pages.home');
});
Route::get('about', function()
{
    return View::make('pages.about');
});
Route::get('store', function()
{
    return View::make('pages.store');
});
Route::get('contact', function()
{
    return View::make('pages.contact');
});

Route::get('insert',function()
{
  return View::make('crud.insert');
}
)->name('insert');

Route::get('/show','PagesController@show');

Route::get('/insert_category', 'CategoriesController@create');

Route::POST('/insert_category', 'CategoriesController@store');

//*****************************************************************

/*Route::get('/', [
    'uses' => 'ProductController@getIndex',
    'as' => 'product.index'
]);*/

Route::get('/add-to-cart/{id}',[
    'uses' => 'ProductController@getAddToCart',
    'as' => 'product.addToCart'
]);

Route::get('/shopping-cart',[
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingCart'
])->middleware('auth');

Route::get('/checkout', [
    'uses' => 'ProductController@getCheckout',
    'as' => 'checkout'
]);

Route::post('/checkout', [
    'uses' => 'ProductController@postCheckout',
    'as' => 'checkout'
]);

Route::get('/reduce/{id}', [
    'uses' => 'ProductController@getReduceByOne',
    'as' => 'product.reduceByOne'
]);
Route::get('/remove/{id}', [
    'uses' => 'ProductController@getRemoveItem',
    'as' => 'product.remove'
]);
